// run_model.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "dump.h"
#include "model.h"
#include "constant.h"

int main (int argc, char* argv[]) {
  
  if (argc != 2) {
    printf("Usage: run_model param_file\n");
    return 1;
  }

  int argi = 0;
  char* paramsFile = argv[++argi];
  
  printf("Reading parameters ...\n");

  FILE* fparams = fopen(paramsFile, "r");
  if (fparams == NULL) {
    printf("ERROR: cannot open the parameter file!\n");
    return 1;
  }

  char line [PF_DIR_SIZE], dumpMode [PF_DIR_SIZE];
  char dumpFile [PF_DIR_SIZE];
  double Mf, Mp;
  double kf, kp;
  double xif, xip, xiperp, xipara;
  double v1, v2, v3, r, dt, p0;
  int nequil, nsteps;
  int lx = 0;
  int ly = 0;
  int nparams = 0;
  int ndumps = 0;
  int nedumps = 0;
  long seed = 0;

  while (fgets(line, sizeof(line), fparams) != NULL) {
    nparams += sscanf(line, "xif = %lf", &xif);
    nparams += sscanf(line, "xip = %lf", &xip);
    nparams += sscanf(line, "xipara = %lf", &xipara);
    nparams += sscanf(line, "xiperp = %lf", &xiperp);
    nparams += sscanf(line, "kf = %lf", &kf);
    nparams += sscanf(line, "kp = %lf", &kp);
    nparams += sscanf(line, "Mf = %lf", &Mf);
    nparams += sscanf(line, "Mp = %lf", &Mp);    
    nparams += sscanf(line, "lx = %d", &lx);
    nparams += sscanf(line, "ly = %d", &ly);
    nparams += sscanf(line, "R = %lf", &r);
    nparams += sscanf(line, "seed = %ld", &seed);
    nparams += sscanf(line, "nsteps = %d", &nsteps);
    nparams += sscanf(line, "nequil = %d", &nequil);
    nparams += sscanf(line, "p0 = %lf", &p0);    
    nparams += sscanf(line, "dt = %lf", &dt);
    nparams += sscanf(line, "v1 = %lf", &v1);
    nparams += sscanf(line, "v2 = %lf", &v2);
    nparams += sscanf(line, "v3 = %lf", &v3);
    
    // Count number of dumps
    if (line[0] != '#' && strstr(line, "dump_") != NULL) {
      if (strstr(line, " equil ") != NULL) {
	nedumps++;
      } else if (strstr(line, " main ") != NULL) {
	ndumps++;
      }
    }
  }

  fclose(fparams);

  if (nparams != 19) {
    printf("ERROR: Not enough parameters specified\n");
    return 1;
  }  
  printf("Read parameters:\n");
  printf("lx = %d\n", lx);
  printf("ly = %d\n", ly);
  printf("R = %g\n", r);  
  printf("xif = %g\n", xif);
  printf("xip = %g\n", xip);
  printf("xipara = %g\n", xipara);
  printf("xiperp = %g\n", xiperp);
  printf("kf = %g\n", kf);
  printf("kp = %g\n", kp);
  printf("Mf = %g\n", Mf);
  printf("Mp = %g\n", Mp);
  printf("p0 = %g\n", p0);  
  printf("dt = %g\n", dt);
  printf("v1 = %g\n", v1);
  printf("v2 = %g\n", v2);
  printf("v3 = %g\n", v3);
  printf("nsteps = %d\n", nsteps);
  printf("nequil = %d\n", nequil);
  printf("nedumps = %d\n", nedumps);
  printf("ndumps = %d\n", ndumps);

  int nthreads = 1;
#ifdef _OPENMP
  nthreads = omp_get_max_threads();
#endif
  printf("nthreads = %d\n", nthreads);
  
  // Read dumps and fixes
  int idump = 0;
  int iedump = 0;
  int printInc, overwrite;
  
  Dump** equilDumps = malloc(sizeof *equilDumps * nedumps);
  Dump** dumps = malloc(sizeof *dumps * ndumps);

  fparams = fopen(paramsFile, "r");
  while (fgets(line, sizeof(line), fparams) != NULL) {
    // Total field dump
    if (sscanf(line, "dump_field %d %d %s %s",
	       &printInc, &overwrite, dumpMode, dumpFile) == 4) {
      if (strcmp(dumpMode, "equil") == 0) {
	equilDumps[iedump] = createFieldDump(dumpFile, printInc, overwrite);
	iedump++;
      } else if (strcmp(dumpMode, "main") == 0) {
	dumps[idump] = createFieldDump(dumpFile, printInc, overwrite);
	idump++;
      }
    }
  } // Close loop over reading parameters
  
  fclose(fparams);
  
  printf("Initialising model ...\n");
  
  Model* model = createModel(lx, ly, seed);
  model->xif = xif;
  model->xip = xip;
  model->xipara = xipara;
  model->xiperp = xiperp;
  model->kf = kf;
  model->kp = kp;
  model->Mf = Mf;
  model->Mp = Mp;
  model->ndumps = nedumps;
  model->dumps = equilDumps;
  model->dt = dt;
  
  initCircle(model, r, p0);
  
  printf("Done initialisation.\n");
  printf("Doing equilibration run ...\n");
  
#ifdef _OPENMP
  double start, end, duration;
  start = omp_get_wtime();
#endif
  run(model, nequil);

#ifdef _OPENMP
  end = omp_get_wtime();
  duration = end-start;
  printf("Time taken (sec): %.5f\n", duration);
  printf("\n");
#endif

  model->ndumps = ndumps;
  model->dumps = dumps;

  // Turn on motility
  model->v1 = v1;
  model->v2 = v2;
  model->v3 = v3;  
  
  printf("Doing main simulation run ...\n");

#ifdef _OPENMP
  start = omp_get_wtime();
#endif

  run(model, nsteps);

#ifdef _OPENMP
  end = omp_get_wtime();
  duration = end-start;
  printf("Time taken (sec): %.5f\n", duration);
#endif
  
  // Clean up
  deleteModel(model);

  for (int i = 0; i < nedumps; i++) {
    deleteDump(equilDumps[i]);
  }
  free(equilDumps);

  for (int i = 0; i < ndumps; i++) {
    deleteDump(dumps[i]);
  }
  free(dumps);
}
