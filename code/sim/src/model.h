// model.h

#ifndef MODEL_H
#define MODEL_H

#include "dump.h"
#include "random.h"

typedef struct Model {
  int lx;
  int ly;
  int getIndex;
  int setIndex;
  double dt;
  double kf, kp;
  double xif, xip, xiperp, xipara;
  double v1, v2, v3;
  double Mf, Mp;
  double** phiField[2];
  double*** polField[2];
  double*** alignField;
  double*** gphiField;
  double** chemPot;
  Random* random;
  int ndumps;
  Dump** dumps;
} Model;

Model* createModel(int lx, int ly, long seed);
void deleteModel(Model* model);

void initCircle(Model* model, double r, double p);
void startUpdateField(Model* model);
void endUpdateField(Model* model);
void update(Model* model);
void run(Model* model, int nsteps);
void output(Model* model, int step);

#endif
