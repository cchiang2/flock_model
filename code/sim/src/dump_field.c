// dump_field.c
// Dump the entire field

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "dump.h"
#include "model.h"
#include "util.h"

typedef struct FieldDump {
  Dump super; // Base struct must be the first element
  bool overwrite;
} FieldDump;

void fieldOutput(FieldDump* dump, Model* model, int step) {
  char tmpfile [PF_DIR_SIZE];
  if (dump->overwrite) {
    strcpy(tmpfile, dump->super.filename);
    strcat(tmpfile, ".tmp");
  } else {
    strcpy(tmpfile, dump->super.filename);
    char suffix [80];
    sprintf(suffix, ".%d", step);
    strcat(tmpfile, suffix);
  }
  FILE* f = fopen(tmpfile, "w");
  int get = model->getIndex;
  for (int i = 0; i < model->lx; i++) {
    for (int j = 0; j < model->ly; j++) {
      fprintf(f, "%d %d %g %g %g %g\n", i, j, model->phiField[get][i][j],
	      model->polField[get][i][j][0], model->polField[get][i][j][1],
	      model->polField[get][i][j][2]);
    }
    fprintf(f, "\n");
  }
  fclose(f);
  if (dump->overwrite) {
    rename(tmpfile, dump->super.filename);
  }
}

void deleteFieldDump(FieldDump* dump) {
  free(dump);
}

DumpFuncs fieldDumpFuncs =
  {
   .output = (void (*)(Dump*, Model*, int)) &fieldOutput,
   .destroy = (void (*)(Dump*)) &deleteFieldDump
  };

Dump* createFieldDump(char* filename, int printInc, bool overwrite) {
  FieldDump* dump = malloc(sizeof *dump);
  setDump(&dump->super, dump, filename, printInc, &fieldDumpFuncs);
  dump->overwrite = overwrite;
  return (Dump*) dump;
}
