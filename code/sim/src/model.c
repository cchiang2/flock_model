// model.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "dump.h"
#include "model.h"
#include "util.h"
#include "array.h"
#include "random.h"
#include "constant.h"

Model* createModel(int lx, int ly, long seed) {
  Model* model = malloc(sizeof *model);
  model->lx = lx;
  model->ly = ly;
  model->setIndex = 0;
  model->getIndex = 0;

  // Default parameter values
  model->dt = 1.0;
  model->Mf = 1.0;
  model->Mp = 1.0;
  model->kf = 1.0;
  model->kp = 1.0;
  model->xif = 1.0;
  model->xip = 1.0;
  model->xiperp = 1.0;
  model->xipara = 1.0;
  model->v1 = 0.0;
  model->v2 = 0.0;
  model->v3 = 0.0;

  for (int i = 0; i < 2; i++) {
    model->phiField[i] = create2DDoubleArray(model->lx, model->ly);
    model->polField[i] = create3DDoubleArray(model->lx, model->ly, 3);
  }
  model->gphiField = create3DDoubleArray(model->lx, model->ly, 2);
  model->chemPot = create2DDoubleArray(model->lx, model->ly);
  model->alignField = create3DDoubleArray(model->lx, model->ly, 3);
  
  // Init random generator
  model->random = createRandom(seed);
  
  // Dumps
  model->dumps = NULL;
  model->ndumps = 0;
  
  return model;
}

void deleteModel(Model* model) {
  if (model != NULL) {
    for (int i = 0; i < 2; i++) {
      free(model->phiField[i]);
      free(model->polField[i]);
    }
    free(model->gphiField);
    free(model->chemPot);
    free(model->alignField);
    free(model->random);
    free(model);
  }
}

void initCircle(Model* model, double r, double p) {
  double x0 = model->lx/2.0;
  double y0 = model->ly/2.0;
  double r2 = r*r;
  double twopi = 2.0*PF_PI;
  for (int i = 0; i < model->lx; i++) {
    double dx = i+0.5-x0;
    for (int j = 0; j < model->ly; j++) {
      double dy = j+0.5-y0;
      if (dx*dx+dy*dy <= r2) {
	model->phiField[0][i][j] = 1.0;
	model->phiField[1][i][j] = 1.0;
	double theta = twopi*0.01*randDouble(model->random);
	double px = p*cos(theta);
	double py = p*sin(theta);
	double psq = px*px+py*py;
	model->polField[0][i][j][0] = px;
	model->polField[0][i][j][1] = py;
	model->polField[0][i][j][2] = psq;
	model->polField[1][i][j][0] = px;
	model->polField[1][i][j][1] = py;
	model->polField[1][i][j][2] = psq;
      } else {
	double theta = twopi*randDouble(model->random);
	double a = 0.01;
	double px = a*cos(theta);
	double py = a*sin(theta);
	double psq = px*px+py*py;
	model->polField[0][i][j][0] = px;
	model->polField[0][i][j][1] = py;
	model->polField[0][i][j][2] = psq;
	model->polField[1][i][j][0] = px;
	model->polField[1][i][j][1] = py;
	model->polField[1][i][j][2] = psq;	
      }
    }
  }
}

void run(Model* model, int nsteps) {
  for (int n = 0; n <= nsteps; n++) {
    output(model, n);
    update(model);
  }
}

void output(Model* model, int step) {
  if (step % 1000 == 0) {
    printf("Step %d\n", step);
    for (int i = 0; i < model->ndumps; i++) {
      dumpOutput(model->dumps[i], model, step);
    }
  }
}

void update(Model* model) {

  startUpdateField(model);
  
  int lx = model->lx;
  int ly = model->ly;
  int set = model->setIndex;
  int get = model->getIndex;
  double kf = model->kf;
  double kp = model->kp;
  double xifsq = model->xif*model->xif;
  double xipsq = model->xip*model->xip;  
  double xiparasq = model->xipara*model->xipara;
  double xiperp = model->xiperp;
  double Mf = model->Mf;
  double Mp = model->Mp;
  double dt = model->dt;
  double v1 = model->v1;
  double v2 = model->v2;
  double v3 = model->v3;

  double** field = model->phiField[get];
  double*** pfield = model->polField[get];
  double*** afield = model->alignField;
  double*** gfield = model->gphiField;
  double** chemPot = model->chemPot;

  // Compute gradient and alignment
  for (int i = 0; i < lx; i++) {
    int iu = iup(lx,i);
    int iuu = iup(lx,iu);
    int id = idown(lx,i);
    int idd = idown(lx,id);
    for (int j = 0; j < ly; j++) {
      int ju = iup(ly,j);
      int juu = iup(ly,ju);
      int jd = idown(ly,j);
      int jdd = idown(ly,jd);

      double gphix = grad(i,j,iuu,iu,id,idd,0,field);
      double gphiy = grad(i,j,juu,ju,jd,jdd,1,field);
      gfield[i][j][0] = gphix;
      gfield[i][j][1] = gphiy;      

      double px = pfield[i][j][0];
      double py = pfield[i][j][1];
      pfield[i][j][2] = px*px+py*py;
      
      double gpdot = gphix*px+gphiy*py;
      afield[i][j][0] = gpdot*px;
      afield[i][j][1] = gpdot*py;
      afield[i][j][2] = gpdot;
    }
  }
 
  for (int i = 0; i < lx; i++) {
    int iu = iup(lx,i);
    int iuu = iup(lx,iu);
    int id = idown(lx,i);
    int idd = idown(lx,id);
    for (int j = 0; j < ly; j++) {
      int ju = iup(ly,j);
      int juu = iup(ly,ju);
      int jd = idown(ly,j);
      int jdd = idown(ly,jd);
      
      double phi = field[i][j];
      double px = pfield[i][j][0];
      double py = pfield[i][j][1];
      double psq = pfield[i][j][2];

      // Grads and divs
      double gphix = gfield[i][j][0];
      double gphiy = gfield[i][j][1];
      double divp = cgrad(i,j,iuu,iu,id,idd,0,0,pfield) +
	cgrad(i,j,juu,ju,jd,jdd,1,1,pfield);
      
      // Chemical potential
      double cahn = 2.0 * kf * (phi*(phi-1.0)*(2.0*phi-1.0) -
				xifsq*lapl(i,j,iu,id,ju,jd,field));

      double pol = - kp * (4.0*psq + xiperp*divp + 2.0*xiparasq *
			   (cgrad(i,j,iuu,iu,id,idd,0,0,afield) +
			    cgrad(i,j,juu,ju,jd,jdd,1,1,afield)));
      
      chemPot[i][j] = cahn + pol;

      // Molecular field
      double prePol = 4.0*(1.0-2.0*phi+psq);
      double preAlign = xiperp+2.0*xiparasq*afield[i][j][2];
      double hx = kp * (prePol*px - 2.0*xipsq*clapl(i,j,iu,id,ju,jd,0,pfield) +
			preAlign*gphix);
      double hy = kp * (prePol*py - 2.0*xipsq*clapl(i,j,iu,id,ju,jd,1,pfield) +
			preAlign*gphiy);

      // Update polar field
      // (p grad) p
      double advx = cupwind(i,j,iuu,iu,id,idd,0,0,px,pfield) +
	cupwind(i,j,juu,ju,jd,jdd,0,1,py,pfield);
      double advy = cupwind(i,j,iuu,iu,id,idd,1,0,px,pfield) +
	cupwind(i,j,juu,ju,jd,jdd,1,1,py,pfield);
      
      // p (div p)
      double pxdivp = divp * px;
      double pydivp = divp * py;
      
      // grad p^2
      double gpsqx = cgrad(i,j,iuu,iu,id,idd,2,0,pfield);
      double gpsqy = cgrad(i,j,juu,ju,jd,jdd,2,1,pfield);

      // Noise
      double preNoise = 0.0; //sqrt(6.0*Mp*dt);
      double nx = preNoise*(randDouble(model->random)*2.0-1.0);
      double ny = preNoise*(randDouble(model->random)*2.0-1.0);

      double pxnew = px - dt*(v1*advx + v2*pxdivp + v3*gpsqx + Mp*hx - nx);
      double pynew = py - dt*(v1*advy + v2*pydivp + v3*gpsqy + Mp*hy - ny);
      model->polField[set][i][j][0] = pxnew;
      model->polField[set][i][j][1] = pynew;
      model->polField[set][i][j][2] = pxnew*pxnew+pynew*pynew;

      // Partial update of phi field
      model->phiField[set][i][j] = phi -
	dt*v1*(upwind(i,j,iuu,iu,id,idd,0,px,field) +
	       upwind(i,j,juu,ju,jd,jdd,1,py,field) + phi*divp);      
    }
  }

  // Update phi field
  for (int i = 0; i < lx; i++) {
    int iu = iup(lx,i);
    int id = idown(lx,i);
    for (int j = 0; j < ly; j++) {
      int ju = iup(ly,j);
      int jd = idown(ly,j);
      model->phiField[set][i][j] += dt*Mf*lapl(i,j,iu,id,ju,jd,chemPot);
    }
  }
  
  endUpdateField(model);
}

void startUpdateField(Model* model) {
  model->setIndex = model->getIndex == 1 ? 0 : 1;
}

void endUpdateField(Model* model) {
  model->getIndex = model->setIndex;
}
