// util.h
// Utility math and periodic boundary functions

#ifndef UTIL_H
#define UTIL_H

// For periodic boundaries
int iwrap(int len, int i);
int iup(int len, int i);
int idown(int len, int i);
int idiff(int len, int i1, int i2);
double ddiff(double len, double d1, double d2);

// For taking derivatives
double lapl(int i, int j, int iu, int id, int ju, int jd, double** field);
double clapl(int i, int j, int iu, int id, int ju, int jd, int ic,
	     double*** field);
double grad(int i, int j, int uu, int u, int d, int dd, int comp, 
	    double** field);
double cgrad(int i, int j, int uu, int u, int d, int dd, int ic, int oc, 
	     double*** field);
double upwind(int i, int j, int uu, int u, int d, int dd, int comp, 
	      double v, double** field);
double cupwind(int i, int j, int uu, int u, int d, int dd, int ic, int oc, 
	       double v, double*** field);
#endif
