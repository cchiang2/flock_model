// dump.h

#ifndef DUMP_H
#define DUMP_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Threshold for the number of chars in a file directory
#ifndef PF_DIR_SIZE
#define PF_DIR_SIZE 500
#endif

#ifndef PF_HAS_ARMA
#define PF_HAS_ARMA 1
#endif

struct Model;
struct Dump;

typedef struct DumpFuncs {
  void (*output)(struct Dump* dump, struct Model* model, int step);
  void (*destroy)(struct Dump* dump);
} DumpFuncs;

typedef struct Dump {
  DumpFuncs* funcs;
  void* derived;
  char* filename;
  int printInc;
} Dump;

void dumpOutput(Dump* dump, struct Model* model, int step);
void deleteDump(Dump* dump);
void setDump(Dump* dump, void* derived, char* filename, int printInc,
	     DumpFuncs* funcs);

Dump* createFieldDump(char* filename, int printInc, bool overwrite);
#endif
